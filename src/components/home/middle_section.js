import { Avatar, Button, Grid, Typography } from '@mui/material'
import { styled } from "@mui/material"

import React from 'react'
import { OutlinedIconButton } from '../base/buttons'
import { LefFolderComponent, RightFolderComponent } from '../base/folders'
import PlayArrowIcon from '@mui/icons-material/PlayArrow';
import CustomInput from '../base/input/custom_input'

const BannerImage = styled('img')(({ theme }) => ({
    width: "100%",
    height: 'auto',
    objectFit: 'cover'
}))
const InputContainer = styled('div')(({ theme }) => ({
    position: 'absolute',
    bottom: '20px',
    right: '20px',
    width: '160px'
}))
const BottomItem = styled('div')(({ theme }) => ({
    height: '200px',
    display: 'flex',
    flexDirection: 'column',
    padding: '50px 0'
}))
const BottomTitle = styled('div')(({ theme }) => ({
    display: 'flex',
    alignItems: 'center'
}))
const TeamContainer = styled('div')(({ theme }) => ({
    display: 'flex',
    marginLeft: '20px',
    flexDirection: 'row-reverse'
}))
const StyledAvatar = styled(Avatar)(({ theme, transform }) => ({
    borderWidth: '3px',
    borderColor: 'black',
    width: '50px',
    height: '50px',
    borderStyle: 'solid',
    transform: transform ? `translateX(${transform * 10}px)` : 'none'
}))
const avatarsItems = [
    {
    url: '/images/avatars/avatar1.jpg'
},
    {
    url: '/images/avatars/avatar2.jpg'
},
    {
    url: '/images/avatars/avatar3.jpg'
},
]

const MiddleSection = () => {
    return (
        <Grid container spacing={3}>
            <Grid xs={12} md={8} lg={8} xl={8} item sx={{ position: 'relative' }}>
                <LefFolderComponent />
                <InputContainer>
                    <CustomInput />
                </InputContainer>

            </Grid>
            <Grid xs={12} md={4} lg={4} xl={4} item>
                <RightFolderComponent />

            </Grid>
            <Grid xs={12} md={6} lg={6} xl={6} item>
                <BottomItem>
                    <OutlinedIconButton>
                        <PlayArrowIcon sx={{ color: "#fff" }} />
                    </OutlinedIconButton>
                    <Typography variant='caption' sx={{ paddingLeft: '50px', width: '120px' }}>
                        lets see how it works
                    </Typography>
                </BottomItem>
            </Grid>
            <Grid xs={12} md={6} lg={6} xl={6} item>
                <BottomItem>
                    <BottomTitle>
                        <Button sx={{height: '40px'}} variant="outlined">
                            Team
                        </Button>
                        <TeamContainer>
                            {
                                avatarsItems.map((item, key) => <StyledAvatar key={key} src={item.url} transform={key}/>)
                            }
                        </TeamContainer>
                    </BottomTitle>
                    <Typography variant='h6' sx={{marginTop: '20px'}}>
                            A COMPLETE FINANCIAL SYSTEM, INCLUDING MODULE FOR PAYMENTS SYSTEM, BANKING,ACCOUNT MANAGEMENT, DIGITAL ASSET SETTELMENT, COMERCIAL LOAN PROCCESSING, 
                    </Typography>
                </BottomItem>
            </Grid>
        </Grid>
    )
}

export default MiddleSection