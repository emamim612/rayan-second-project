import { styled, Typography } from "@mui/material"
import CircleIcon from '@mui/icons-material/Circle';
import CircleOutlinedIcon from '@mui/icons-material/CircleOutlined';
import { BaseButton } from "../base/buttons";

const SubHeadetContainer = styled('div')(({ theme }) => ({
    display: 'grid',
    paddingTop: '10px',
    gridTemplateColumns: '60% 20% 20%',
    MarginBottom: '40px'
}))
const ButtonContainer = styled('div')(({ theme }) => ({
    width: '100%',
    flexDirection: 'row-reverse',
    display: 'flex',
    height: '40px'
}))

const SubHeader = () => {
    return (
        <SubHeadetContainer>
            <Typography maxWidth={'200px'} color="text.primary" variant="h5">
                REAL-TIME MANAGEMENT AND ANALYSIS
            </Typography>
            <Typography maxWidth={'100px'} color="text.primary" variant="body1" component="p">
                HIGH
                &nbsp;
                <CircleOutlinedIcon sx={{ fontSize: '8px', padding: 0 }} /> <CircleIcon sx={{ fontSize: '8px', padding: 0 }} />
                &nbsp;
                QUALITY
            </Typography>
            <ButtonContainer>
                <BaseButton variant="contained">
                    Get started
                </BaseButton>
            </ButtonContainer>

        </SubHeadetContainer>
    )
}

export default SubHeader