import { Button, IconButton, styled } from "@mui/material"

export const BaseButton = styled(Button)(({ theme }) => ({
    fontWeight: 'bold',
    fontSize: theme.typography.caption.fontSize,
    textTransform: 'none'
}))
export const OutlinedIconButton = styled(IconButton)(({ theme }) => ({
    borderStyle: 'solid',
    borderWidth: '1px',
    borderColor: 'white',
    width: '40px',
    height: '40px'
}))