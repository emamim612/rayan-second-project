import { styled, Container} from "@mui/material"

const BaseContainer = styled(Container)(({ theme }) => ({
    paddingTop: '40px'
}))

export default BaseContainer
