import { ThemeProvider as MuiThemeProvider, createTheme } from '@mui/material/styles';

export const themeObj = {
  typography: {
      fontFamily: [
          '-apple-system',
          'BlinkMacSystemFont',
          '"Segoe UI"',
          'Roboto',
          '"Helvetica Neue"',
          'Arial',
          'sans-serif',
          '"Apple Color Emoji"',
          '"Segoe UI Emoji"',
          '"Segoe UI Symbol"',
      ].join(','),
  },
  palette: {
      primary: {
          main: '#E8F1FF',
          seconary: '#1062D0',
          light: '#E8F1FF'
      },
      success: {
        main: '#24AF22',
        light: '#E7F5E9'
      },
      error: {
          main: '#CB0B0B',
      },
      warning: {
        main: '#D68000'
      },
      text: {
          primary: '#fff',
          secondary: '#6F6F6F',
          disabled: '#000'
      },
  },
  shape: {
    borderRadius: 20
  }
  
}

const theme = createTheme(themeObj);



const AppThemeProvider = ({ children }) => {
    return (
        <MuiThemeProvider theme={theme}>
            {children}
        </MuiThemeProvider>
    )
}

export default AppThemeProvider