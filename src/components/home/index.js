import { Typography } from '@mui/material'
import { cornerGeneratore } from '../../helpers/helper'
import Header from './header'
import MiddleSection from './middle_section'
import SubHeader from './sub_header'



const HomePage = () => {
    return (
        <>
            <Header />
            <SubHeader/>
            <MiddleSection/>
        </>
    )
}

export default HomePage