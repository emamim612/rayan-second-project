import { useEffect } from 'react';
import BaseContainer from '../src/components/base/layouts/baseLayout'
import AppThemeProvider from '../src/providers/AppThemeProvider'
import '../styles/globals.css'

function MyApp({ Component, pageProps }) {
  useEffect(() => {
    const jssStyles = document.querySelector('#jss-server-side');

    if (jssStyles) {
      jssStyles?.parentElement?.removeChild(jssStyles);
    }
  }, []);
  return (
    <AppThemeProvider>
      <BaseContainer maxWidth="lg">
        <Component {...pageProps} />
      </BaseContainer>
    </AppThemeProvider>
  )
}

export default MyApp
