import { styled, Typography } from "@mui/material"

const HeaderContainer = styled('div')(({ theme }) => ({
    display: 'grid',
    gridTemplateColumns: '60% 20% 20%',
    borderColor: 'white',
    height: '40px',
    borderBottomStyle: 'solid',
    borderTopStyle: 'solid',
    borderWidth: '1px'
}))

const HeaderItem = styled(Typography)(({ theme }) => ({
    width: '100%',
    display: 'flex',
    alignItems: 'center'
}))

const Header = () => {
    return (
        <HeaderContainer>
            <HeaderItem variant="caption" color="text.primary">
                GEOMAN.AI
            </HeaderItem>
            <HeaderItem variant="caption" color="text.primary">
                SOLUTIONS
            </HeaderItem>
            <HeaderItem variant="caption" sx={{
                direction: 'rtl'
            }} color="text.primary">
                PROJECTS
            </HeaderItem>
        </HeaderContainer>
    )
}

export default Header