import { IconButton, styled, Typography } from "@mui/material"
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';

const CustomInputContainer = styled('div')(({ theme }) => ({
  height: '40px',
  display: 'flex',
  alignItems: 'center',
  borderStyle: 'solid',
  borderWidth: '1px',
  borderRadius: '20px',
  borderColor: 'black',
  width: '100%',
  padding: '0 6px',
  justifyContent: 'space-between'
}))
const ActionBotton = styled(IconButton)(({ theme }) => ({
  borderStyle: 'solid',
  borderWidth: '1px',
  backgroundColor: 'black',
  width: '30px',
  height: '30px'
}))

const CustomInput = () => {
  return (
    <CustomInputContainer>
      <Typography variant="caption" color="black" fontWeight="bold">
        Get expert advice
      </Typography>
      <ActionBotton>
        <ArrowForwardIcon sx={{fontSize: '15px', color: "white"}}/>
      </ActionBotton>
    </CustomInputContainer>
  )
}

export default CustomInput