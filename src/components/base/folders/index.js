import { styled } from "@mui/material"
import { cornerGeneratore } from "../../../helpers/helper"

export const FolderContainer = styled('div')(({ theme }) => ({
    width: '100%'
}))
export const Folder = styled('div')(({ theme }) => ({
    height: 'fit-content',
    borderRadius: '20px 20px 20px 20px',
    position: 'relative',
}))
export const LeftFolder = styled(Folder)(({ theme }) => ({
    backgroundColor: 'unset',
    borderRadius: '20px 0 20px 20px',
    height: '500px',
    overflow: 'hidden'
}))

export const RightFolder = styled(Folder)(({ theme }) => ({
    backgroundColor: 'white',
    borderRadius: '0 20px 20px 20px',
    height: '500px',
    overflow: 'hidden'
}))

const LeftFolderBoforeContainer = styled('div')(({ theme }) => ({
    display: 'flex',
    justifyContent: 'flex-end',
    width: '100%',
}))
const RightFolderBoforeContainer = styled('div')(({ theme }) => ({
    display: 'flex',
    width: '100%',
}))
const LeftFolderBofore = styled('div')(({ theme }) => ({
    backgroundColor: '#ffef03',
    borderRadius: '30px 20px 0 0',
    height: '50px',
    overflow: 'hidden',
    width: '40%',
    transform: 'translateY(30px) translateX(-1px)'
}))
const RightFolderBofore = styled('div')(({ theme }) => ({
    backgroundColor: 'white',
    borderRadius: '20px 30px 0 0',
    height: '50px',
    overflow: 'hidden',
    width: '40%',
    transform: 'translateY(30px)'
}))

const BannerImage = styled('img')(({ theme }) => ({
    width: "100%",
    height: 'auto',
    objectFit: 'cover'
}))

export const LefFolderComponent = ({ src }) => {
    return (
        <FolderContainer>
            <LeftFolderBoforeContainer>
                <LeftFolderBofore>
                </LeftFolderBofore>
            </LeftFolderBoforeContainer>
            <LeftFolder>
                <BannerImage src="/images/person2.jpg" />
            </LeftFolder>
        </FolderContainer>
    )
}
export const RightFolderComponent = ({ src }) => {
    return (
        <FolderContainer>
            <RightFolderBoforeContainer>
                <RightFolderBofore/>
            </RightFolderBoforeContainer>
            <RightFolder>
                <BannerImage src="/images/picture2.jpg" />
            </RightFolder>
        </FolderContainer>
    )
}
