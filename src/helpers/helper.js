export const cornerGeneratore = (x, y, points, unit, radius, vertical, horizontal) => {
    try {
        
        const output = ''
        const interval = Math.PI / (2 * points)
        const verticalSign = vertical === 'top' ? '+' : '-'
        const horizontalSign = horizontal === 'left' ? '+' : '-'
        for (let i = 0; i < Math.PI / 2; i += interval) {
            if (vertical === 'bottom' && horizontal === 'right') {
                const dx = Math.sin(i) * radius
                const dy = (1 - Math.cos(i)) * radius
                output += `, calc(${x} - ${dx}${unit}) calc(${y} - ${dy}${unit})`
            } else if(vertical === 'top' && horizontal === 'right') {

                const dx = Math.sin(i) * radius
                const dy = (1 - Math.cos(i)) * radius
                output += `, calc(${x} + ${dx}${unit}) calc(${y} + ${dy}${unit})`
            } else {

                const dx = (1 - Math.cos(i)) * radius
                const dy = (1 - Math.sin(i)) * radius
                output += `, calc(${x} ${horizontalSign} ${dx}${unit}) calc(${y} ${verticalSign} ${dy}${unit})`
            }

        }
        return output
        

    } catch (e) {
        return ''
    }
}